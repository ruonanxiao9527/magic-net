﻿

using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;

using SqlSugar;
using System;
using System.Threading.Tasks;

namespace Magic.Core.Service;

/// <summary>
/// 操作日志服务
/// </summary>
public class SysOpLogService : ISysOpLogService
{
    private readonly SqlSugarRepository<SysLogOp> _sysOpLogRep; // 操作日志表仓储

    public SysOpLogService(SqlSugarRepository<SysLogOp> sysOpLogRep)
    {
        _sysOpLogRep = sysOpLogRep;
    }

    /// <summary>
    /// 分页查询操作日志
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task<PageList<SysLogOp>> PageList(QueryOpLogPageInput input)
    {
        var opLogs = await _sysOpLogRep.AsQueryable()
           .WhereIF(!string.IsNullOrWhiteSpace(input.Name), u => u.Name.Contains(input.Name.Trim()))
           .WhereIF(Enum.IsDefined(typeof(YesOrNot),input.Success), u => u.Success == input.Success)
           .WhereIF(!string.IsNullOrWhiteSpace(input.SearchBeginTime), u => u.OpTime >= DateTime.Parse(input.SearchBeginTime.Trim()) && u.OpTime <= DateTime.Parse(input.SearchEndTime.Trim()))
           .OrderBy(u => u.Id, OrderByType.Desc)
           .ToPagedListAsync(input.PageNo, input.PageSize);

        return opLogs.McPagedResult();
    }

    /// <summary>
    /// 清空操作日志
    /// </summary>
    /// <returns></returns>
    public async Task Clear()
    {
        await _sysOpLogRep.DeleteAsync(m => true);
    }
}
