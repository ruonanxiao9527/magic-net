﻿using System.Collections.Generic;

namespace Magic.Core.Service;

/// <summary>
/// 员工信息参数
/// </summary>
public class EmpOutput
{
    /// <summary>
    /// 员工Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 工号
    /// </summary>
    public string JobNum { get; set; }

    /// <summary>
    /// 机构id
    /// </summary>
    public string OrgId { get; set; }

    /// <summary>
    /// 机构名称
    /// </summary>
    public string OrgName { get; set; }

    /// <summary>
    /// 机构与职位信息
    /// </summary>
    public List<EmpExtOrgPosOutput> ExtOrgPos { get; set; } = new List<EmpExtOrgPosOutput>();

    /// <summary>
    /// 职位信息
    /// </summary>
    public List<EmpPosOutput> Positions { get; set; } = new List<EmpPosOutput>();

    /// <summary>
    /// 附属机构
    /// </summary>
    public List<EmpExtOrgPosOutput> ExtIds { get; set; } = new List<EmpExtOrgPosOutput>();

    /// <summary>
    /// 职位集合
    /// </summary>
    public List<long> PosIdList { get; set; } = new List<long>();
}
