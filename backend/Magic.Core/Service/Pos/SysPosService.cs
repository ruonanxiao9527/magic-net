﻿using Furion.FriendlyException;
using Magic.Core.Entity;
using Mapster;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

/// <summary>
/// 职位服务
/// </summary>
public class SysPosService : ISysPosService
{
    private readonly SqlSugarRepository<SysPos> _sysPosRep;  // 职位表仓储   

    private readonly ISysEmpPosService _sysEmpPosService;
    private readonly ISysEmpExtOrgPosService _sysEmpExtOrgPosService;

    public SysPosService(SqlSugarRepository<SysPos> sysPosRep,
                         ISysEmpPosService sysEmpPosService,
                         ISysEmpExtOrgPosService sysEmpExtOrgPosService)
    {
        _sysPosRep = sysPosRep;
        _sysEmpPosService = sysEmpPosService;
        _sysEmpExtOrgPosService = sysEmpExtOrgPosService;
    }

    /// <summary>
    /// 分页获取职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task<PageList<SysPos>> PageList(QueryPosPageInput input)
    {
        var pos = await _sysPosRep.AsQueryable()
                                .WhereIF(!string.IsNullOrWhiteSpace(input.Name), u => u.Name.Contains(input.Name.Trim()))
                                .WhereIF(!string.IsNullOrWhiteSpace(input.Code), u => u.Code.Contains(input.Code.Trim()))
                                .Where(u => u.Status == CommonStatus.ENABLE).OrderBy(u => u.Sort)
                                .ToPagedListAsync(input.PageNo, input.PageSize);
        return pos.McPagedResult();
    }

    /// <summary>
    /// 获取职位列表
    /// </summary>
    /// <returns></returns>
    public async Task<List<SysPos>> List(QueryPosPageInput input)
    {
        return await _sysPosRep.AsQueryable()
                                                 .WhereIF(!string.IsNullOrWhiteSpace(input.Code), u => u.Code.Contains(input.Code.Trim()))
                                                 .Where(u => u.Status != CommonStatus.DELETED)
                                                 .OrderBy(u => u.Sort).ToListAsync();
    }

    /// <summary>
    /// 增加职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task Add(AddPosInput input)
    {
        var isExist = await _sysPosRep.AnyAsync(u => u.Name == input.Name || u.Code == input.Code);
        if (isExist)
            throw Oops.Oh(ErrorCode.D6000);

        var pos = input.Adapt<SysPos>();
        await _sysPosRep.InsertAsync(pos);
    }

    /// <summary>
    /// 删除职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task Delete(Core.PrimaryKeyParam input)
    {
        // 该职位下是否有员工
        var hasPosEmp = await _sysEmpPosService.HasPosEmp(input.Id);
        if (hasPosEmp)
            throw Oops.Oh(ErrorCode.D6001);

        // 该附属职位下是否有员工
        var hasExtPosEmp = await _sysEmpExtOrgPosService.HasExtPosEmp(input.Id);
        if (hasExtPosEmp)
            throw Oops.Oh(ErrorCode.D6001);

        await _sysPosRep.DeleteAsync(u => u.Id == input.Id);
    }

    /// <summary>
    /// 更新职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task Update(EditPosInput input)
    {
        var isExist = await _sysPosRep.AnyAsync(u => (u.Name == input.Name || u.Code == input.Code) && u.Id != input.Id);
        if (isExist)
            throw Oops.Oh(ErrorCode.D6000);

        var pos = input.Adapt<SysPos>();
        await _sysPosRep.AsUpdateable(pos).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommandAsync();
    }

    /// <summary>
    /// 获取职位
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public async Task<SysPos> Get(Core.PrimaryKeyParam input)
    {
        return await _sysPosRep.FirstOrDefaultAsync(u => u.Id == input.Id);
    }
}
