﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysTenantService : ITransient
{
    Task Add(AddTenantInput input);
    Task Delete(PrimaryKeyParam input);
    Task<SysTenant> Get(PrimaryKeyParam input);
    Task GrantMenu(GrantTenantMenuInput input);
    Task<List<OwnMenuOutput>> OwnMenu(PrimaryKeyParam input);
    Task<PageList<SysTenant>> PageList(QueryTenantPageInput input);
    Task ResetUserPwd(PrimaryKeyParam input);
    Task Update(EditTenantInput input);
}
