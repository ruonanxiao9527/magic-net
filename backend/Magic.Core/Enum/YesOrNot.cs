﻿using System.ComponentModel;

namespace Magic.Core;

/// <summary>
/// YesOrNot
/// </summary>
public enum YesOrNot
{
    /// <summary>
    /// 是
    /// </summary>
    [Description("是")]
    Y = 0,

    /// <summary>
    /// 否
    /// </summary>
    [Description("否")]
    N = 1
}
