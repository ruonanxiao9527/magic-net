﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core
{
    public enum ArticleType
    {
        /// <summary>
        /// 数据页面帮助手册
        /// </summary>
        [Description("首页页面")]
        首页页面 = 1,

        /// <summary>
        /// 数据页面帮助手册
        /// </summary>
        [Description("数据页面")]
        数据页面 = 2,
    }
}
