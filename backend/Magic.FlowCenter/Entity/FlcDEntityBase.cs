﻿using Magic.Core.Entity;
using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;

namespace Magic.FlowCenter.Entity;

/// <summary>
/// 自定义实体基类
/// </summary>
[Tenant("1")]
public abstract class FlcDEntityBase : EntityBase
{
   
}

/// <summary>
/// 递增主键实体基类
/// </summary>
public abstract class AutoIncrementEntity
{
    /// <summary>
    /// 主键Id
    /// </summary>
    [SugarColumn(IsIdentity = true, ColumnDescription = "Id主键", IsPrimaryKey = true)] //通过特性设置主键和自增列 
    // 注意是在这里定义你的公共实体
    public virtual int Id { get; set; }
}

/// <summary>
/// 主键实体基类
/// </summary>
public abstract class PrimaryKeyEntity
{
    /// <summary>
    /// 主键Id
    /// </summary>
    [SugarColumn(ColumnDescription = "Id主键", IsPrimaryKey = true)]
    // 注意是在这里定义你的公共实体
    public virtual long Id { get; set; }
}
