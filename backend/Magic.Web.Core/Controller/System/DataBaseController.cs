﻿using Furion.DynamicApiController;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using System.Collections.Generic;

namespace Magic.Web.Core;

/// <summary>
/// 数据库管理
/// </summary>
[ApiDescriptionSettings(Name = "DataBase", Order = 100, Tag = "数据库管理")]
public class DataBaseController : IDynamicApiController
{
    private readonly IDataBaseManageService _service;
    public DataBaseController(IDataBaseManageService service)
    {
        _service = service;
    }

    /// <summary>
    /// 添加列
    /// </summary>
    /// <param name="input"></param>
    [HttpPost("/column/add")]
    public void ColumnAdd(DbColumnInfoInput input)
    {
        _service.ColumnAdd(input);
    }

    /// <summary>
    /// 删除列
    /// </summary>
    /// <param name="input"></param>
    [HttpPost("/column/delete")]
    public void ColumnDelete(DbColumnInfoOutput input)
    {
        _service.ColumnDelete(input);
    }

    /// <summary>
    /// 编辑列
    /// </summary>
    /// <param name="input"></param>
    [HttpPost("/column/edit")]
    public void ColumnEdit(EditColumnInput input)
    {
        _service.ColumnEdit(input);
    }

    /// <summary>
    /// 获取表字段
    /// </summary>
    /// <param name="tableName"></param>
    /// <returns></returns>
    [HttpGet("/dataBase/columnInfoList")]
    public List<DbColumnInfoOutput> GetColumnInfosByTableName(string tableName)
    {
        return _service.GetColumnInfosByTableName(tableName);
    }

    /// <summary>
    /// 获取所有表
    /// </summary>
    /// <returns></returns>
    [HttpGet("/dataBase/tableInfoList")]
    public List<DbTableInfo> GetTableInfoList()
    {
        return _service.GetTableInfoList();
    }

    /// <summary>
    /// 新增表
    /// </summary>
    /// <param name="input"></param>
    [HttpPost("/table/add")]
    public void TableAdd(DbTableInfoInput input)
    {
        _service.TableAdd(input);
    }

    /// <summary>
    /// 删除表
    /// </summary>
    /// <param name="input"></param>
    [HttpPost("/table/delete")]
    public void TableDelete(DbTableInfo input)
    {
        _service.TableDelete(input);
    }

    /// <summary>
    /// 编辑表
    /// </summary>
    /// <param name="input"></param>
    [HttpPost("/table/edit")]
    public void TableEdit(EditTableInput input)
    {
        _service.TableEdit(input);
    }

    /// <summary>
    /// 生成实体
    /// </summary>
    /// <param name="input"></param>
    [HttpPost("/table/createEntity")]
    public void CreateEntity(CreateEntityInput input)
    {
        _service.CreateEntity(input);
    }
}
