﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 租户服务
/// </summary>
[ApiDescriptionSettings(Name = "Tenant", Order = 100, Tag = "租户服务")]
public class SysTenantController : IDynamicApiController
{
    private readonly ISysTenantService _service;
    public SysTenantController(ISysTenantService service)
    {
        _service=service;
    }

    /// <summary>
    /// 分页查询租户
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysTenant/page")]
    public async Task<PageList<SysTenant>> PageList([FromQuery] QueryTenantPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 增加租户
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysTenant/add")]
    public async Task Add(AddTenantInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除租户
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysTenant/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新租户
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysTenant/edit")]
    public async Task Update(EditTenantInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 获取租户
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysTenant/detail")]
    public async Task<SysTenant> Get([FromQuery] PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 授权租户管理员角色菜单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysTenant/grantMenu")]
    public async Task GrantMenu(GrantTenantMenuInput input)
    {
        await _service.GrantMenu(input);
    }

    /// <summary>
    /// 获取租户管理员角色拥有菜单Id集合
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysTenant/ownMenu")]
    public async Task<List<OwnMenuOutput>> OwnMenu([FromQuery] PrimaryKeyParam input)
    {
        return await _service.OwnMenu(input);
    }

    /// <summary>
    /// 重置租户管理员密码
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysTenant/resetPwd")]
    public async Task ResetUserPwd(PrimaryKeyParam input)
    {
        await _service.ResetUserPwd(input);
    }
}
