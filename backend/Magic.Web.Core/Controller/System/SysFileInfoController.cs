﻿using Aliyun.OSS;
using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Magic.Web.Core;

/// <summary>
/// 文件服务
/// </summary>
[ApiDescriptionSettings(Name = "File", Order = 100, Tag = "文件服务")]
public class SysFileInfoController : IDynamicApiController
{
    private readonly ISysFileService _service;
    public SysFileInfoController(ISysFileService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页获取文件列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysFileInfo/page")]
    public async Task<dynamic> PageList([FromQuery] QueryFilePageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 获取文件列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysFileInfo/list")]
    public async Task<List<SysFile>> List([FromQuery] QueryFilePageInput input)
    {
        return await _service.List(input);
    }

    /// <summary>
    /// 删除文件
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysFileInfo/delete")]
    public async Task Delete(Magic.Core.PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 获取文件详情
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysFileInfo/detail")]
    public async Task<SysFile> Get([FromQuery] Magic.Core.PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 预览文件
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysFileInfo/preview")]
    public async Task<IActionResult> Preview([FromQuery] Magic.Core.PrimaryKeyParam input)
    {
        return await _service.Preview(input);
    }

    /// <summary>
    /// 上传文件
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    [HttpPost("/sysFileInfo/upload")]
    public async Task UploadFileDefault(IFormFile file)
    {
        await _service.UploadFileDefault(file);
    }
    /// <summary>
    /// 下载文件
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysFileInfo/download")]
    public async Task<IActionResult> Download([FromQuery] Magic.Core.PrimaryKeyParam input)
    {
        return await _service.DownloadFileInfo(input);
    }

    /// <summary>
    /// 上传头像
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    [HttpPost("/sysFileInfo/uploadAvatar")]
    public async Task<long> UploadFileAvatar(IFormFile file)
    {
        return await _service.UploadFileAvatar(file);
    }

    /// <summary>
    /// 上传文档
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    [HttpPost("/sysFileInfo/uploadDocument")]
    public async Task UploadFileDocument(IFormFile file)
    {
        await _service.UploadFileDocument(file);
    }

    /// <summary>
    /// 上传富文本图片
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    [HttpPost("/sysFileInfo/uploadEditor")]
    public async Task<string> UploadFileEditor(IFormFile file)
    {
        return await _service.UploadFileEditor(file);
    }

    /// <summary>
    /// 上传商店图片
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    [HttpPost("/sysFileInfo/uploadShop")]
    public async Task UploadFileShop(IFormFile file)
    {
        await _service.UploadFileShop(file);
    }
}
