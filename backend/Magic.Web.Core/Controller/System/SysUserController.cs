﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 用户服务
/// </summary>
[ApiDescriptionSettings(Name = "User", Order = 100, Tag = "用户服务")]
public class SysUserController : IDynamicApiController
{
    private readonly ISysUserService _service;
    public SysUserController(ISysUserService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询用户
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysUser/page")]
    public async Task<PageList<UserOutput>> PageList([FromQuery] QueryUserPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 增加用户       
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/add")]
    public async Task Add(AddUserInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除用户
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新用户
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/edit")]
    public async Task Update(EditUserInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 查看用户
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysUser/detail")]
    public async Task<UserOutput> Get([FromQuery] PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 修改用户状态
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/changeStatus")]
    public async Task ChangeUserStatus(ChangeUserStatusInput input)
    {
        await _service.ChangeUserStatus(input);
    }

    /// <summary>
    /// 授权用户角色
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/grantRole")]
    public async Task GrantUserRole(GrantUserInput input)
    {
        await _service.GrantUserRole(input);
    }

    /// <summary>
    /// 授权用户数据范围
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/grantData")]
    public async Task GrantUserData(GrantUserInput input)
    {
        await _service.GrantUserData(input);
    }

    /// <summary>
    /// 更新用户信息
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/updateInfo")]
    public async Task UpdateUserInfo(UpdateUserInfo input)
    {
        await _service.UpdateUserInfo(input);
    }

    /// <summary>
    /// 修改用户密码
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/updatePwd")]
    public async Task UpdateUserPwd(ChangePasswordUserInput input)
    {
        await _service.UpdateUserPwd(input);
    }

    /// <summary>
    /// 获取用户拥有角色
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysUser/ownRole")]
    public async Task<dynamic> GetUserOwnRole([FromQuery] PrimaryKeyParam input)
    {
        return await _service.GetUserOwnRole(input);
    }

    /// <summary>
    /// 获取用户拥有数据
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysUser/ownData")]
    public async Task<dynamic> GetUserOwnData([FromQuery] PrimaryKeyParam input)
    {
        return await _service.GetUserOwnData(input);
    }
    /// <summary>
    /// 重置用户密码
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/resetPwd")]
    public async Task ResetUserPwd(PrimaryKeyParam input)
    {
        await _service.ResetUserPwd(input);
    }

    /// <summary>
    /// 修改用户头像
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/updateAvatar")]
    public async Task UpdateAvatar(UploadAvatarInput input)
    {
        await _service.UpdateAvatar(input);
    }

    /// <summary>
    /// 获取用户选择器
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysUser/selector")]
    public async Task<dynamic> GetUserSelector([FromQuery] QueryUserPageInput input)
    {
        return await _service.GetUserSelector(input);
    }

    /// <summary>
    /// 用户导出
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysUser/export")]
    public async Task<IActionResult> Export()
    {
        return await _service.Export();
    }

    /// <summary>
    /// 用户导入
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    [HttpPost("/sysUser/import")]
    public async Task Import(IFormFile file)
    {
        await _service.Import(file);
    }
}
